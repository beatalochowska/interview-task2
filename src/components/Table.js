import React, { Component } from 'react'
import MaterialTable from 'material-table'

class Table extends Component {
    constructor(props) {
      super(props);
      this.state = {
        columns: [
          { title: 'Avatar', field: 'imageUrl', render: rowData => <img alt={'table'} src={rowData.imageUrl} style={{width: 80}}/> },
          { title: 'Name', field: 'name' },
          { title: 'Category', field: 'category' },
          { title: 'Price', field: 'price', type: 'numeric' },
          { title: 'Description', field: 'description' },
          { title: 'Comments', field: 'comments' },
        ],
        data: [
          { name: 'table', category: 'furnitures', price: 120, description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', comments: 'ok', imageUrl: 'https://static.abra-meble.pl/catalog/product/a/b/abra_17_06_28_nora_stol_sonoma_1_.jpg' },
          { name: 'book', category: 'entertainment', price: 20, description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', comments: 'intersting', imageUrl: 'https://dictionary.cambridge.org/es/images/thumb/book_noun_001_01679.jpg?version=4.0.90' },
          { name: 'lamp', category: 'furnitures', price: 200, description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', comments: 'ok', imageUrl: 'https://dictionary.cambridge.org/es/images/thumb/book_noun_001_01679.jpg?version=4.0.90'},
          { name: 't-shirt', category: 'clothes', price: 50, description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', comments: 'ok', imageUrl: 'https://dictionary.cambridge.org/es/images/thumb/book_noun_001_01679.jpg?version=4.0.90'},
          { name: 'table', category: 'furnitures', price: 120, description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', comments: 'ok', imageUrl: 'https://static.abra-meble.pl/catalog/product/a/b/abra_17_06_28_nora_stol_sonoma_1_.jpg' },
          { name: 'book', category: 'entertainment', price: 20, description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', comments: 'intersting', imageUrl: 'https://dictionary.cambridge.org/es/images/thumb/book_noun_001_01679.jpg?version=4.0.90' },
          { name: 'lamp', category: 'furnitures', price: 200, description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', comments: 'ok', imageUrl: 'https://dictionary.cambridge.org/es/images/thumb/book_noun_001_01679.jpg?version=4.0.90'},
          { name: 't-shirt', category: 'clothes', price: 50, description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', comments: 'ok', imageUrl: 'https://dictionary.cambridge.org/es/images/thumb/book_noun_001_01679.jpg?version=4.0.90'},
        ]
      }
    }
  
    render() {
      return (
        <MaterialTable
          title="Online Market"
          columns={this.state.columns}
          data={this.state.data}
          options={{
            sorting: true,
            filtering: true
          }}
          editable={{
            onRowAdd: newData =>
              new Promise((resolve, reject) => {
                setTimeout(() => {
                  {
                    const data = this.state.data;
                    data.push(newData);
                    this.setState({ data }, () => resolve());
                  }
                }, 1000)
              }),
            onRowUpdate: (newData, oldData) =>
              new Promise((resolve, reject) => {
                setTimeout(() => {
                  {
                    const data = this.state.data;
                    const index = data.indexOf(oldData);
                    data[index] = newData;
                    this.setState({ data }, () => resolve());
                  }
                }, 1000)
              }),
            onRowDelete: oldData =>
              new Promise((resolve, reject) => {
                setTimeout(() => {
                  {
                    let data = this.state.data;
                    const index = data.indexOf(oldData);
                    data.splice(index, 1);
                    this.setState({ data }, () => resolve());
                  }
                }, 1000)
              }),
            
          }}
        />
      )
    }
  }

  export default Table;